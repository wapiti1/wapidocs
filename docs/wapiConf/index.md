# WapiConf

## Présentation

Ce script réalisé avec python 3 permet de réaliser rapidement des configurations pour promtail a partir de templates (Nginx, symfony...).

Il fonctionne en complément de wapitail et n'est qu'un outil pour simplifier la création de fichiers de configuration. Pour plus de détails concernant la configuration de promtail, veuillez vous référer a la documentation de [promtail {{ config.extra.promtail_vers }}](https://grafana.com/docs/loki/{{ config.extra.promtail_vers }}/clients/promtail/){target="_blank"} ou encore à la page [wapiTail](../wapiTail/index.md).

---

## Installation

### Avec poetry

Pour installer poetry, vous pouvez vous référer aux documentations suivantes: [https://python-poetry.org/](https://python-poetry.org/){target="_blank"}   
Ensuite, il vous suffit d’exécuter les commandes:

```bash linenums="1"
cd wapiconf
poetry install
make init
```

### Avec pip

Il vous faut installer les paquets `click` et `pyyaml`:   
`pip install -U click pyyaml`

---

## Utilisation

WapiConf permet de créer et de gérer une configuration promtail qui sera créer a la racine du dossier `wapiconf`.   
Pour éditer une configuration déjà existante, vous pouvez aussi copier un fichier promtail.yaml au même endroit et utiliser l'option `manage`.

Enfin pour déployer la configuration promtail, il suffit de la mettre à la place de `wapiTail/config/promtail.yaml`.

### Avec wapiconf.py

Cette méthode fonctionne sans installer poetry. Il vous faut cependant installer les dépendances requises (voir [Installation avec pip](#avec-pip))

`python3 wapiconf.py`: Affiche une aide   
`python3 wapiconf.py gen`: Permet de générer un fichier promtail.yaml   
`python3 wapiconf.py manage`: Permet de gérer (Ajouter ou supprimer) des pipelines dans le fichier de configuration actuel   
`python3 wapiconf.py list`: Liste les templates disponibles

### Avec Make

`make run`: Affiche une aide   
`make run-gen`: Permet de générer un fichier promtail.yaml   
`make run-manage`: Permet de gérer (Ajouter ou supprimer) des pipelines dans le fichier de configuration actuel   
`make run-list`: Liste les templates disponibles
