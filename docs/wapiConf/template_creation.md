# Création de templates

Vous pouvez voir les templates déjà existantes pour avoir des exemples de syntaxes.  
Le schéma des templates est faite à partir d'une config yaml séparé en deux parties principales : [template_fields](#template_fields) et [template](#template)

## template_fields

Le nom du champ est utilisé comme nom de catégorie.
Cette partie décrit les champs à changer (`pos`), leur description (`helper`) ainsi que la valeur par défaut du champ (`default_value`).   
La description des champs est un tableau contenant le chemin a la configuration visée avec chaque champ séparé par des ".".

Exemple : `pos: ['job_name', 'static_configs.labels.job']` qui correspond aux champs :

```yaml linenums="1"
job_name: valeur
```

et

```yaml linenums="1"
static_configs:
    labels:
        job: valeur
```

ou valeur sera remplacé soit par la valeur par défaut, soit par la valeur entrée par l'utilisateur.   
Pour une configuration permettant alors de modifier un chemin de fichier on obtient :

```yaml linenums="1"
template_fields:
  chemin_fichier: 
    pos: ['static_configs.labels.__path__']
    default_value: "/mon/chemin/de/fichier"
    helper: "Le chemin du fichier"
```

## template

Cette partie correspond a la configuration qui sera traité et utilisé pour compléter le fichier de configuration final.
Elle contient uniquement une configuration "générale" ainsi que les parties à remplacer.   
La syntaxe de remplacement utilisé est celle utilisée par le templating de python pour les template nommés (voir [la documentation officielle](https://docs.python.org/3/library/string.html#template-strings){target="_blank"})

Par exemple, pour la valeur `chemin_fichier`, on devra par exemple écrire comme paramètre: `__path__: /hostfs${path}`.

Nous obtenons par exemple la config suivante pour les champs `name` et `path`:

```yaml linenums="1"
template:
  - job_name: ${name}
    static_configs:
      - targets:
        - localhost
        labels:
          job: myJob
          __path__: /hostfs${path}
```

Vous pouvez obtenir des exemples de configuration ainsi que les paramètres des pipelines sur la documentation de [promtail (Scraping)](https://grafana.com/docs/loki/{{ config.extra.promtail_vers }}/clients/promtail/scraping/){target="_blank"} ainsi que sur [promtail (Pipelines)](https://grafana.com/docs/loki/{{ config.extra.promtail_vers }}/clients/promtail/pipelines/){target="_blank"}.
