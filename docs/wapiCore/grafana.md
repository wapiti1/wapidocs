# Grafana

Grafana possède une interface graphique qui peut être assez compliqué à prendre en main, c'est pourquoi ce tutoriel va vous permettre de configurer votre instance grafana et vous aider à créer votre premier dashboard[^1].

On suppose que vous avez donc lancé votre serveur avec l'aide de la commande `make wapiCore-run` ou `make wapiCore-run-silent`.   
Grafana est alors disponible a l'adresse `localhost:3000` ou celle que vous avez configuré avec nginx.

## Compte administrateur

Grafana intègre un compte administrateur par défaut lors du premier démarrage de ce dernier. On peut en effet retrouver ces lignes suivantes dans le fichier `wapiCore/config/grafana/grafana.ini` :

```ini linenums="208"
#################################### Security ####################################
[security]
# disable creation of admin user on first start of grafana
;disable_initial_admin_creation = false

# default admin user, created on startup
;admin_user = admin

# default admin password, can be changed before first start of grafana,  or in profile settings
;admin_password = admin
```

Libre à vous de les modifier en fonction de vos besoins. Pour ce tutoriel nous garderons les valeurs par défaut (Login: `admin` et mdp: `admin`).

Lors de la première connexion, si vous utilisez les credentials par défaut, grafana vous proposera ensuite de le remplacer par un autre pour des raisons de sécurité.

## Interface


### Dashboard principal
![Page principale de grafana](../assets/img/wapiCore/grafana_home.png)

#### Menu principal

!!! info ""
    ![Le menu principal de grafana](../assets/img/wapiCore/grafana_main_menu.png){ align=right }
    Dans le menu à gauche, vous avez :

    - **Search** : Pour rechercher un dashboard sur grafana
    - **Create** : Pour créer un nouvel élément (dashboard, dossier, importer des éléments)
    - **Dashboard** : Permet d'accéder aux différents dashboards disponibles.
    - **Explore** : Permet de consulter directement les donnés retourné par les différentes sources actuellement configuré sur grafana.
    - **Alerting** : Permet de configurer les alertes ainsi que les différents canaux de notification.
    - **Configuration** : Permet de changer les différents paramètres de grafana.
    - **Server Admin** : Regroupe les paramètres administrateur tels que la gestion des utilisateurs, des groupes ou de voir les paramètres actuellement configuré sur grafana.

#### Menu d'édition

!!! info ""
    ![Le menu d'édition de grafana](../assets/img/wapiCore/grafana_edit_menu.png){ align=right }
    En haut à droite, vous trouverez les éléments suivant :

    - Ajouter un panel.
    - Sauvegarder le dashboard.
    - Paramètres du dashboard.
    - Naviguer entre les différentes vues.

#### Menu panel

!!! info ""
    ![Le menu d'édition d'un panel de grafana](../assets/img/wapiCore/grafana_panel_menu.png){ align=right }
    En haut de chaque panel, vous avez la possibilité de cliquer pour révéler le menu ci-dessus :

    - Voir le panel (Affiche uniquement le panel sélectionné)
    - Éditer le panel
    - Partager le panel
    - Inspecter le panel (Permet d'afficher et de modifier le code JSON du panel)
    - Plus d'options (Dupliquer le panel, le copier ou en faire un objet de librairie)
    - Supprimer le panel

### Consulter les donnés sur loki

Pour ce faire, vous pouvez vous rendre sur la page `explore` :
![La page explore](../assets/img/wapiCore/grafana_explore.png)

Vous pouvez sélectionner une source de donnés sur laquelle réaliser les requêtes avec le menu déroulant en haut de l'écran.  
Pour consulter les donnés de loki, il suffit de le choisir comme source (La source `Loki - wapiCore`).  

Nous obtenons ainsi l'outil de query suivant :
![L'outil de query de loki](../assets/img/wapiCore/grafana_loki.png)

Ici nous pouvons alors soit écrire une query au format [logQL](https://grafana.com/docs/loki/{{ config.extra.loki_vers }}/logql){target="_blank"} soit utiliser l'assistant de query en cliquant sur le bouton `Log browser`.

Pour un bref rappel sur les requêtes logQL, vous pouvez aussi cliquer sur le bouton `Help`.


[^1]: Un dashboard est une page permettant de synthétiser des informations. Ces informations peuvent se présenter sous forme de graphiques ou de chiffres par exemple.