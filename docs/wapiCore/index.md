# WapiCore

## Présentation

Il s'agit du cœur de wapiti. Ce wrapper permet de configurer, lancer et administrer un serveur loki ainsi qu'un serveur grafana prêt a l'emploi.  
WapiCore utilise docker et docker-compose ce qui rend son fonctionnement ainsi que les configurations faciles à exporter.

### Loki ({{ config.extra.loki_vers }})

Loki est un outil permettant d'indexer des méta-donnés de logs et de les découper en chunks. Cette méthode permet une indexation rapide ainsi qu'un stockage et une utilisation de ressource peu élevé.  
Pour récupérer des informations depuis loki, c'est aussi simple que de réaliser une requêtes au format [logQL](https://grafana.com/docs/loki/{{ config.extra.loki_vers }}/logql){target="_blank"} sur les différents stream[^1] configuré (Loki utilise une méthode de "brute force" pour récupérer les donnés, mais cette dernière reste relativement rapide).   
Cette structure de donné ainsi que cette approche permet alors à loki d'être léger et de consommer moins de ressource par rapport à ses concurrents tels que la suite *logstash + elasticsearch*.  

Un des inconvénients de loki est l'impossibilité d'ajouter des logs de date antérieure à celle de la dernière entrée d'un stream. Ceci est une conséquence directe de son architecture.

La méthode de stockage de loki peut se résumer ainsi:
![Processus de stockage des logs par loki](../assets/img/wapiCore/chunks_diagram.png)
[Source](https://grafana.com/){target="_blank"}


Pour plus de détail sur le fonctionnement de loki, vous pouvez lire la page [architecture](https://grafana.com/docs/loki/{{ config.extra.loki_vers }}/architecture/){target="_blank"}.

### Grafana ({{ config.extra.grafana_vers }})

Grafana est l'interface utilisateur permettant de réaliser des requêtes à notre serveur Loki ainsi que de visualiser lesdits logs.  
Grafana intègre un système de dashboard et est entièrement customisable.   
Il intègre aussi un système d'alerte ce qui peut être pratique pour détecter les anomalies si, par exemple, le nombre de logs d'erreur par minute dépasse une certaine valeur.  

![Exemple de vue de l'interface de grafana](../assets/img/wapiCore/grafana.png)
[Source](https://grafana.com/){target="_blank"}

---

## Installation

### Prérequis

Vous devez installer `docker` version 17.12.0 (ou plus) ainsi que `docker-compose` version 1.18.0 (ou plus) :

Les commandes suivant permet d'installer docker-compose 1.29.0:


```bash linenums="1"
curl -L https://github.com/docker/compose/releases/download/1.29.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

Pour installer docker, veuillez vous référer a la page officielle d'installation de [docker](https://docs.docker.com/engine/install/ubuntu/){target="_blank"}

### Configuration

Vous trouverez les fichiers de configuration de loki et de grafana dans les dossiers `wapiCore/config/loki/` et `wapiCore/config/grafana/` respectivement.  

#### Configuration de Loki

La configuration par défaut permet d'avoir un serveur local écoutant sur le port 3100.   
La configuration actuelle de loki permet aussi de :

- Avoir une instance standalone (A l'instar d'une configuration en cluster, pour des raison de simplicité de déploiement)
- Ne pas avoir de système d'authentification (L'authentification est géré par nginx si vous utilisez la configuration donné ci-dessous)
- Rejeter les logs datant de plus de 168h (Modifiable avec les paramètres `reject_old_samples` et `reject_old_samples_max_age`)
- Stocker les donnés de loki dans le dossier `wapiCore/config/loki/loki-filesystem`

Ce dernier est aussi exposé uniquement sur localhost:3100 de part la configuration de son container dans le fichier `wapiCore/dockerfiles/loki.yaml`.  

Il est recommandé d'utiliser un proxy SSL+basic_auth avec nginx en ajoutant la configuration suivante :  

??? tldr "loki.conf"
    ```sh linenums="1"
    server {
        # Loki devient accessible en https sur l'addresse loki.example.com.
        listen                  443 ssl;
        server_name             loki.example.com;

        # Configuration des certificats
        ssl_certificate         /path/to/fullchain.pem;
        ssl_certificate_key     /path/to/privkey.pem;

        # Proxy trafic externe https vers port 3100 interne + basic auth
        location / {
            auth_basic                            "Loki api";
            auth_basic_user_file                  path/to/http_pwd;
            proxy_pass                            "http://localhost:3100";
            proxy_set_header Host                 $http_host;
            proxy_set_header X-Real-IP            $remote_addr;
            proxy_set_header X-Forwarded-For      $proxy_add_x_forwarded_for;
        }
    }
    ```

#### Configuration de grafana

La configuration de grafana se trouve dans le fichier `wapiCore/config/grafana/custom.ini`.   
Elle contient tout les paramètres utilisable par grafana ainsi que la description de ces derniers.  
Il est à noter que les variables telles que les chemins sont géré par des variables d'environnement inscrites dans `wapiCore/dockerfiles/grafana.yaml`

La configuration actuelle de grafana permet de stocker les donnés de grafana dans le dossier `wapiCore/config/grafana-filesystem`.   
Vous trouverez à cet endroit les donnés persistantes de grafana telles que:

- Les logs de grafana se trouvant dans `logs`
- Les donnés de grafana dans `data`
- Le dossier d'installation des plugins de grafana dans `plugins`
- Le dossier provisioning permettant d'installer rapidement des configurations pour grafana[^2] dans `provisioning` ainsi que la configuration de l'instance loki dans le cas ou nous exécutons directement wapiCore.

Une fois wapicore installé puis lancé, vous pouvez vous référer a la page [grafana](./grafana.md) affin de pouvoir faire connaissance avec l'interface de grafana ainsi que de créer vos premiers dashboards.

Il est recommandé d'utiliser un proxy https avec nginx en ajoutant la configuration suivante :  

??? tldr "grafana.conf"
    ```sh linenums="1"
    ## Force redirection from http to https
    server {
        listen                  80;
        server_name             grafana.example.com;

        return 301 https://$host$request_uri;
    }

    ## Server configuration
    server {
        listen                  443 ssl;
        server_name             grafana.example.com;

        # Configuration des certificats
        ssl_certificate         /path/to/fullchain.pem;
        ssl_certificate_key     /path/to/privkey.pem;

        # Proxy externe https vers le port 3000 interne
        location / {
            proxy_pass          "http://localhost:3000";
            proxy_set_header Host                 $http_host;
            proxy_set_header X-Real-IP            $remote_addr;
            proxy_set_header X-Forwarded-For      $proxy_add_x_forwarded_for;
        }
    }
    ```

#### Configuration avancé

Pour une configuration avancé, vous pouvez vous référer aux documentations de [loki](https://grafana.com/docs/loki/{{ config.extra.loki_vers }}/configuration){target="_blank"} ainsi que celles de [grafana](https://grafana.com/docs/grafana/{{ config.extra.grafana_vers }}/administration/configuration){target="_blank"}.

---

## Utilisation

!!! info
    `*` est à remplacer par l'une des possibilités suivantes:

    - wapiCore : La suite complète loki + grafana.
    - loki : Permet de lancer uniquement un serveur loki.
    - grafana : Permet de lancer uniquement un serveur grafana.

`make *-run`: Exécute le programme demandé.   
`make *-run-silent`: Exécute le programme sans attacher le terminal courant au container docker.   
`make *-stop`: Stoppe le programme et son container docker.   
`make *-rm`: Stoppe et supprime le container docker du programme.    
`make *-shell`: Permet d'ouvrir un shell dans le container docker du programme.

`make network-create` : Permet d'initialiser le réseau virtuel docker   
`make network-rm` : Permet de supprimer le réseau virtuel docker

`make init` : Permet d'initialiser les dossier qui contiendront les donnés de grafana et de loki   
`make clear-filesystems` : Permet de supprimer le contenu des dossier de donnés de grafana et de loki

`make clean`: Supprime le container docker, le contenu des dossiers de donnés et le réseau virtuel.

[^1]: Un stream est une chaîne de donnés partageant les mêmes labels. (Voir image sur le fonctionnement de loki)
[^2]: Voir la [documentation de grafana](https://grafana.com/docs/grafana/{{ config.extra.grafana_vers }}/administration/provisioning/){target="_blank"} au sujet du dossier provisioning.