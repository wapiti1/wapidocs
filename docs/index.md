# Wapiti

![Wapiti](./assets/img/logo.png)

## Présentation

Ce projet est un wrapper permettant de déployer, configurer et administrer facilement la stack Loki-grafana-promtail.  
Cette stack permet d’envoyer, traiter, stocker et d’afficher des statistiques sur des logs applicatifs et logs système en provenance de différentes sources.

wapiCore et wapiTail ont une similaire avec un makefile, un dossier config et des docker-files soit à la racine, soit dans un dossier docker-files.   
Veuillez penser à créer vos fichier de configuration, vous pouvez vous inspirer des fichier *.example* en plus de la documentation du module pour vous aider.

Ce projet se décompose en 3 parties:

1. [WapiCore](./wapiCore/index.md): Le cœur de wapiti. WapiCore permet de déployer rapidement un serveur loki-grafana pré-configuré avec docker.

2. [WapiTail](./wapiTail/index.md): WapiTail est un wrapper de promtail permettant de déployer simplement un serveur promtail via docker. Promtail est l’agrégateur de logs qui se charge d’envoyer ces derniers à loki. 

3. [WapiConf](./wapiConf/index.md): Un générateur de fichier configuration permettant de simplifier la configuration de promtail

## Exemple de configuration

Wapiti peut se configurer de la manière suivante :

![Illustration d'une configuration wapiti](./assets/img/wapiti.png)

L'instance promtail (Géré par wapiTail) de la machine 1 récupère les logs des services 1 et 2 et les envoi au serveur Loki (Géré par wapiCore). Cette communication est chiffré et se fait via l'adresse loki.example.eu .  
On considère que le proxy nginx est configuré pour rediriger le trafic du port 443 de cette adresse au port 3100 sur le réseau interne (localhost).

Les logs sont alors redirigé sur le container docker exécutant Loki qui va se charger de stocker les logs reçu.

Il est important de noter que les containers loki et grafana sont relié entre eux par un réseau virtuel ce qui leur permet une communication rapide et sécurisé (réseau local et spécifique à ces deux containers, non-exposé à internet).

Enfin, il est maintenant possible d'accéder aux logs stocké par loki grace à Grafana. Ce dernier est accessible sur le site grafana.example.eu depuis internet.

