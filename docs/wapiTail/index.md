# WapiTail

## Présentation

WapiTail est un wrapper de promtail permettant de configurer et de le déployer sur une machine.   
Promtail sera alors chargé d'agréger et de formater les logs venant de diverses sources (SYSlog, évènements windows, fichier de logs...) et de les envoyer au serveur loki qui sera géré par wapiCore.

La version de promtail actuellement utilisé est la {{ config.extra.promtail_vers }} .

## Installation

### Prérequis

Vous devez installer `docker` version 17.12.0 (ou plus) ainsi que `docker-compose` version 1.18.0 (ou plus) :

Les commandes suivant permet d'installer docker-compose 1.29.0:

```bash linenums="1"
curl -L https://github.com/docker/compose/releases/download/1.29.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

Pour installer docker, veuillez vous référer a la page officielle d'installation de [docker](https://docs.docker.com/engine/install/ubuntu/){target="_blank"}

### Configuration

Il est possible de changer le nom du container (utiles si vous voulez exécuter plusieurs instances promtail sur un seul serveur) en modifiant la variable `CONTAINER_NAME` dans le fichier `wapiTail/Makefile`.

Vous pouvez utiliser [WapiConf](../wapiConf/index.md) pour vous aider à la configuration de promtail.

La configuration de promtail se trouve dans le fichier `wapiTail/config/promtail.yaml`, celle actuellement fournis se décompose ainsi :

- **server** : Contient la configuration de la partie serveur de promtail. Notamment le point d'accès a l'API de ce dernier. 
- **positions** : Contient un fichier temporaire sauvegardant l'état d'avancement de l'agent promtail   
- **clients** : Contient l'URL vers l'API de loki ainsi que les informations de connection si une authentification est nécessaire.   
- **scrape_configs** : Contient la configuration sur la récupération et le traitement des logs a envoyer à l'instance Loki. 

Pour la configuration de **scrape_configs**, vous trouverez une explication en bref ci-dessous ainsi qu'une explication plus complète sur la [documentation officielle](https://grafana.com/docs/loki/{{ config.extra.promtail_vers }}/clients/promtail/configuration/)  

??? example "scrape_configs"
    Exemple de configuration avec commentaires explicatifs :
    ```yaml linenums="1"
    scrape_configs:

      # job_name est le nom donné a ce pipeline
      - job_name: symfony-myApp

        # Static_config permet de configurer la cible
        # ainsi que les labels concernant tout les éléments de ce pipeline
        static_configs:            

          # Paramètre imposé par la construction de promtail basé sur prometheus. Ce paramètre n'as pas à être changé.
          - targets:
              - localhost

            # On définit ici tout les labels présent sur chaque éléments sortant de ce pipeline
            labels:

              # Exemple avec un label job nous permetant de facilement retrouver les logs provenant de ce pipeline
              job: symfony

              # Ce label particulier permet d'indiquer quels logs nous allons traiter. 
              # Ici, les logs venant de /var/log/symfony/myapp. Plus d'informations sur les patterns: https://github.com/bmatcuk/doublestar#patterns
              # Il est à noter que la machine hôte est monté sur "hostfs" donc pensez à ajouter ce point de montage dans les chemins de fichiers
              __path__: /hostfs/var/log/symfony/myapp/*.log

              # Il est possible d'ajouter autant de label que l'on souhaite. 
              # Cependant il n'est pas recommandé d'en ajouter trop car une cardinalité du nombre de labels trop grande peut causer une réduction des performances de Loki
              app: myApp

        # On définit les différents étages du pipeline affin de formater/traiter les logs. Celà permet notament d'en extraire des informations telles que le niveau de log (Debug, info, warning, error, critical...) ou encore le timestamp.
        pipeline_stages:

          # On enchaine plusieurs étages affin de pouvoir traiter tout les formats de logs possible.
          # Si le premier étage ne match pas avec notre ligne de log alors on continue avec la seconde et ainsi de suite.
          # L'etage regex utilise la syntaxe regexp (https://github.com/google/re2/wiki/Syntax)
          - regex:
              expression:
                  '\[(?P<app>\w*)\]\[(?P<time>\S*)\] \<(?P<channel>\w*)\.(?P<level>\w*)\>: (?:> )?(?P<message>.*) with context: (?P<context>.*) and extra: (?P<extra>.*)'
          - regex:
              expression:
                  '\[(?P<time>\S*)\] \((?P<channel>\w*)\) (?P<level>\w*): (?:> )?(?P<message>.*)'
          - regex:
              expression:
                  '\[(?P<time>\S*)\] (?P<channel>\w*)\.(?P<level>\w*): (?:> )?(?P<message>.*)'
          - regex:
              expression:
                  '\[(?P<time>\S*)\] (?P<level>\w*): (?:> )?(?P<message>.*)'

          # Cet étage de pipeline permet de définir des labels sur l'objet à envoyer a partir des "variables locales" extraites.
          # Celà rend donc persistantes les donnés extraites a partir des étages regex utilisé précédement dans l'objet final envoyé à Loki.
          - labels:
              app:
              channel:
              level:
              message:
              context:
              extra:

          # Le timestamp étant différent, nous devons le changer avec cet étage.
          # Source permet d'indiquer quelle "variable locale" contient la valeur du timestamp
          # format permet d'indiquer comment extraire cette valeur a partir d'une String (voir https://grafana.com/docs/loki/{{ config.extra.promtail_vers }}/clients/promtail/stages/timestamp/)
          - timestamp:
              source: time
              format: RFC3339
    ```

## Utilisation

Sur un modèle similaire a wapiCore, il est possible d’exécuter et de gérer promtail avec les commandes suivantes:

`make promtail-run`: Exécute promtail.   
`make promtail-run-silent`: Exécute promtail sans attacher le terminal courant au container docker.   
`make promtail-stop`: Stoppe promtail et son container docker.   
`make promtail-rm`: Stoppe et supprime le container docker de promtail.   
`make promtail-shell`: Permet d'ouvrir un shell dans le container docker de promtail (utile pour déboguer).   
`make clean`: Supprime le container docker ainsi que le fichier `wapiTail/config/positions.yaml`.